# Alpine VenenuX

VenenuX build ISO image repository for Alpine Linux flavor

It will build a Alpine ISO in same line as Debian, including manpages and forensic commands.

## Build:

You can buil 32 and 64 bits ISO of Alpine VenenuX edge as:

* For 32bit `mkteaiso --profile=./alpine-venenux-edge-x86 --output=./ --debug`
* For 64bit `mkteaiso --profile=./alpine-venenux-edge-x64 --output=./ --debug`

## Dependencies

VenenuX's `makeiso` or `mkteaiso`, any of them can work:

* VenenuX-teaiso: https://codeberg.org/venenux/venenux-teaiso
* Teaiso: https://gitlab.com/tearch-linux/applications-and-tools/teaiso

## Issues

* Alpine installer will need networking active, there's no offline install possible.


#!/usr/bin/env bash

mkdir -p /etc/X11 || true
echo "Xft.antialias: 0" > /etc/X11/Xresources || true
echo "Xft.rgba:      rgb" >> /etc/X11/Xresources
echo "Xft.autohint:  0" >> /etc/X11/Xresources
echo "Xft.hinting:   1" >> /etc/X11/Xresources
echo "Xft.hintstyle: hintslight" >> /etc/X11/Xresources
echo "XTerm*geometry: 180x20+10+10" >> /etc/X11/Xresources
echo "xterm*vt100*geometry: 80x40" >> /etc/X11/Xresources
echo "XTerm*savelines: 1000" >> /etc/X11/Xresources
echo "XTerm*selectToClipboard: true" >> /etc/X11/Xresources
echo "xterm*loginShell: true" >> /etc/X11/Xresources
echo "xterm*charClass: 33:48,35:48,37:48,43:48,45-47:48,64:48,95:48,126:48" >> /etc/X11/Xresources
echo "xterm*eightBitInput: false" >> /etc/X11/Xresources
echo "URxvt*font:xft:Terminus:pixelsize=14" >> /etc/X11/Xresources
echo "xterm*faceName:Terminus:style=Regular:size=14" >> /etc/X11/Xresources
echo "xterm*faceSize: 14" >> /etc/X11/Xresources
